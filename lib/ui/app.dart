import 'package:flutter/material.dart';
import 'package:week_4_form/ui/validators/mixin_validators.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login",
      home: Scaffold(
        appBar: AppBar(title: Text('Login Bar')),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with CommonValidation {
  final formkey = GlobalKey<FormState>();
  late String emailAddress;
  late String password;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldPassword(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            loginButton()
          ],
        ),
      )
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email'
      ),

      validator: validateEmail,

      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),

      validator: validatePassword,

      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(onPressed: (){
      if (formkey.currentState!.validate()) {
        // Call authentication API.
        formkey.currentState!.save();
        print('$emailAddress, Demo only: $password');
      }

      print('Clicked/Touched login button');
    },
        child: Text('Login')
    );
  }
}