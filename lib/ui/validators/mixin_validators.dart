mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return 'Please input a valid email address';
    };

    return null;
  }

  String? validatePassword(String? value) {
    if(value!.length < 8 && validator(value)) {
      return 'Password must contains at least 1 uppercase letter, 1 lowercase letter, 1 special character, and 8 characters long.';
    }

    return null;
  }

  // Checks if password contains at least 1 uppercase letter, 1 lowercase letter, and 1 special character.
  // It has to be at least 8 character in length as well.
  bool validator(String value) {
    String pattern = r'^(?=.*?[A-Za-z])(?=.*?[@$!%*#?&~]){8,}$';
    RegExp regExp = RegExp(pattern);
    return regExp.hasMatch(value);
  }
}